import java.util.Scanner;
 class Pahlawan{
	String nama;
	String keterampilan;

	Pahlawan(String nama,String keterampilan){
		this.nama=nama;
		this.keterampilan=keterampilan;
	}
	public String getNama(){
		return nama;
	}
	public String getKeterampilan(){
		return keterampilan;
	}
}
	class Penyihir extends Pahlawan{
	Penyihir(String nama, String keterampilan){
	super(nama, keterampilan);
	int jumlahMantra;
	}
}
	class Pemburu extends Pahlawan{
	Pemburu(String nama, String keterampilan){
			super(nama, keterampilan);
			int jumlahPanah;
		}
}
	class Kesatria extends Pahlawan{
	Kesatria(String nama, String keterampilan){
			super(nama, keterampilan);
			int jumlahPedang;
		}
}
 abstract class Tantangan{
	 abstract void TampilkanDeskripsi();
}
class TantanganMakhluk extends Tantangan{
	void TampilkanDeskripsi(){
		System.out.println("Tantangan 1 :" );
		System.out.println("Bertemu dengan makhluk aneh di dalam gua.");
	}
}
class TantanganPuzzle extends Tantangan{
	void TampilkanDeskripsi(){
			System.out.println("Tantangan 2 :" );
			System.out.println("Pecahkan teka-teki aneh yang menghalangi jalan.");
	}
}
class TantanganMisterius extends Tantangan{
	void TampilkanDeskripsi(){
			System.out.println("Tantangan 3 :" );
			System.out.println("Temukan jalan keluar dari labirin misterius.");
	}
}
	public class MainPahlawan{
	public static void main(String[] args){
		System.out.println("Daftar Pahlawan :"+"\n");

		Penyihir penyihir = new Penyihir("Agung","Penyihir");
		System.out.print("1. "+penyihir.getNama()+" - " +penyihir.getKeterampilan()+"\n");

		Pemburu pemburu =new Pemburu("Siti", "Pemburu");
		System.out.print("2. "+pemburu.getNama()+" - " +pemburu.getKeterampilan()+"\n");

		Kesatria kesatria = new Kesatria("Budi", "Kesatria");
		System.out.print("3. "+kesatria.getNama()+" - " +kesatria.getKeterampilan()+"\n");

		TantanganMakhluk tantanganMakhluk = new TantanganMakhluk();
		tantanganMakhluk.TampilkanDeskripsi();
		System.out.println("Jumlah Mantera : 10"+"\n");

		TantanganPuzzle tantanganPuzzle = new TantanganPuzzle();
		tantanganPuzzle.TampilkanDeskripsi();
		System.out.println("Jumlah Panah : 50"+"\n");

		TantanganMisterius tantanganMisterius = new TantanganMisterius();
		tantanganMisterius.TampilkanDeskripsi();
		System.out.println("Jumlah Pedang : 2"+"\n");
	}
}